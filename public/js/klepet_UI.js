/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}

function dodajSlike(vhodnoBesedilo) {
  vhodnoBesedilo = vhodnoBesedilo.split(" ");
  var slike = "";
  for (var i=0; i<vhodnoBesedilo.length; i++) {
    if ((vhodnoBesedilo[i].indexOf("http") == 0) && vhodnoBesedilo[i].indexOf(".jpg" || ".png" || ".gif") > -1) {
      slike += "<br /><img style =\"width:200px; margin-left:20px\" src =\"" + vhodnoBesedilo[i] + "\" /> ";
    }
  }
  vhodnoBesedilo = vhodnoBesedilo.join(" ") + slike;
  return vhodnoBesedilo;
}

/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  var jePoke = sporocilo.indexOf("&#9756;") > -1;
  var jeSlika = sporocilo.indexOf(".jpg" || ".png" || ".gif") > -1;
  if (jeSmesko) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  } else if (jePoke) {
    return divElementHtmlTekst(sporocilo);
  } else if (jeSlika) {
    return divElementHtmlTekst(sporocilo);
  } else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  sporocilo = dodajSlike(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}

var vzdevkiUporabnikov = [];
var nadimkiUporabnikov = [];

/*global dodajNadimek */
function dodajNadimek(ime, nadimek) {
  for (var i in vzdevkiUporabnikov){
    if(vzdevkiUporabnikov[i] == ime){
      nadimkiUporabnikov[i] = nadimek;
    }
  }
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";

// Počakaj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    var napisano = sporocilo.besedilo.split(":");
    var jeNadimek = false;
    var id = -1;
    for (var i in vzdevkiUporabnikov) {
      if ((vzdevkiUporabnikov[i] == napisano[0]) && nadimkiUporabnikov[i] != "") {
        jeNadimek = true;
        id = i;
      }
    }
    var novElement;
    var a = sporocilo.besedilo.substring(napisano[0].length, sporocilo.besedilo.length);
    if (!jeNadimek) {
      novElement = divElementEnostavniTekst(sporocilo.besedilo);
    } else {
      novElement = divElementEnostavniTekst(nadimkiUporabnikov[id] + " (" + vzdevkiUporabnikov[id] + ") " + a);
    }
    $('#sporocila').append(novElement);
  });
  
  socket.on('preimenovanje', function(rezultat) {
    var vhod = rezultat.besedilo.split(" ");
    for (var i in vzdevkiUporabnikov) {
      if (vzdevkiUporabnikov[i] == vhod[0]) {
        vzdevkiUporabnikov[i] = vhod[1];
      }
    }
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    for (var i=0; i < uporabniki.length; i++) {
      var je = false;
      var dodanNadimek = false;
      var id;
      for (var j in vzdevkiUporabnikov) {
        if (vzdevkiUporabnikov[j] == uporabniki[i]) {
          je = true;
          if (nadimkiUporabnikov[j]!="") {
            dodanNadimek = true;
            id = j;
          }
          
        }
      }
      if (!je) {
        vzdevkiUporabnikov.push(uporabniki[i]);
        nadimkiUporabnikov.push("");
      }
      var novElement;
      if (!dodanNadimek) {
        novElement = divElementEnostavniTekst(uporabniki[i]);
      } else {
        novElement = divElementEnostavniTekst(nadimkiUporabnikov[id] + " (" + vzdevkiUporabnikov[id] + ")");
      }
      $('#seznam-uporabnikov').append(novElement);
    }
    
    // Klik na ime uporabnika v seznamu uporabnikov krcne izbranega uporabnika
    $('#seznam-uporabnikov div').click(function() {
      var sistemskoSporocilo = klepetApp.procesirajUkaz("/zasebno \"" + $(this).text() + "\" \"&#9756;\"");
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
